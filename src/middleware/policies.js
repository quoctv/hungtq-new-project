import {errors, jsonError} from '../utils/system';

const authenticated = () => {
  return (req, res, next) => {
    if (!req.principal || !req.principal.owner)
      return res.json(jsonError(errors.NOT_AUTHENTICATED_ERROR));
    return next();
  };
};
export {authenticated};
