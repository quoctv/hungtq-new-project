import {isValidObjectId} from '../utils/validation';
import {errors, jsonError} from '../utils/system';
import {ACCESS_SCOPES, GRANT_TYPES} from '../models/schema/OAuthToken';

const valueRequired = ({attributes}) => {
  return (req, res, next) => {
    let missing = attributes.some((attr) => {
      return !req.body || req.body[attr] === '' || req.body[attr] === undefined || req.body[attr] === null;
    });
    if (missing)
      return res.json(jsonError(errors.MISSING_REQUIRED_VALUE));
    return next();
  };
};

const validObjectId = ({attributes}) => {
  return (req, res, next) => {
    let invalid = attributes.some((attr) => {
      if (!req.body || !req.body[attr])
        return false;
      return !isValidObjectId(req.body[attr]);
    });
    if (invalid)
      return res.json(jsonError(errors.NOT_VALID_ID));
    return next();
  }
};

const validScope = ({attributes}) => {
  const scopes = Object.keys(ACCESS_SCOPES).map(key => ACCESS_SCOPES[key]);
  return (req, res, next) => {
    let invalid = attributes.some((attr) => {
      if (!req.body || !req.body[attr])
        return false;
      return scopes.indexOf(req.body[attr]) === -1;
    });
    if (invalid)
      return res.json(jsonError(errors.INVALID_ACCESS_SCOPE));
    return next();
  }
};

const validGrantType = ({attributes}) => {
  const types = Object.keys(GRANT_TYPES).map(key => GRANT_TYPES[key]);
  return (req, res, next) => {
    let invalid = attributes.some((attr) => {
      if (!req.body || !req.body[attr])
        return false;
      return types.indexOf(req.body[attr]) === -1;
    });
    if (invalid)
      return res.json(jsonError(errors.INVALID_GRANT_TYPE));
    return next();
  }
};

export {valueRequired, validObjectId, validScope, validGrantType};
