'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let jobs = [];
    jobs.push(queryInterface.renameColumn('Users', 'email', 'username'));
    jobs.push(queryInterface.removeColumn('Users', 'firstName'));
    jobs.push(queryInterface.removeColumn('Users', 'lastName'));
    return Promise.all(jobs);
  },
  
  down: (queryInterface, Sequelize) => {
    let jobs = [];
    jobs.push(queryInterface.renameColumn('Users', 'username', 'email'));
    jobs.push(queryInterface.addColumn('Users', 'firstName', {
      type: Sequelize.STRING,
    }));
    jobs.push(queryInterface.addColumn('Users', 'lastName', {
      type: Sequelize.STRING,
    }));
    return Promise.all(jobs);
  }
};
