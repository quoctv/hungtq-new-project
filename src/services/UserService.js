import {OAuth2Client} from 'google-auth-library';
import {comparePassword, hashPassword} from '../utils/encryption';
import {errors, jsonError, jsonSuccess, logger} from '../utils/system';
import {User} from '../models/schema/User';
import {Auth, AUTH_METHODS} from '../models/schema/Auth';
import {Role} from '../models/schema/Role';
import {RolePermission} from '../models/schema/RolePermission';
import {UserRole} from '../models/schema/UserRole';
import {rpcActions as permissions} from '../controllers/RPCControler';
import {S3Service} from './S3Service';
import {Op} from 'sequelize';

const SUPPORTED_OAUTH_PROVIDERS = {
  GOOGLE: 'google',
};

class UserService {

  static async boot() {

    UserService.S3_PROFILE_PHOTO_FOLDER = 'profile-photos';

    UserService.ggClient = new OAuth2Client(getEnv('GG_CLIENT_ID'));

    //-- create default super admin role
    let superAdminRole = await Role.findOne({where: {name: getEnv('SUPER_ADMIN_ROLE')}});
    if (!superAdminRole) {
      superAdminRole = await Role.create({name: getEnv('SUPER_ADMIN_ROLE'), super_user: true, editable: false});
      //-- grant maximum permissions for admin roles
      let adminPermissions = Object.keys(permissions).map(permissionName => {
        let permission = permissions[permissionName];
        let permissionSettings = !!permission.permissionConditions && Object.keys(permission.permissionConditions).map(conditionName => {
          //-- permit without any condition
          return permission.permissionConditions[conditionName].permit();
        });
        return {
          roleId: superAdminRole.id,
          permissionId: permissionName,
          permissionSettings: JSON.stringify(permissionSettings || []),
        }
      });
      await RolePermission.bulkCreate(adminPermissions);
    }

    //-- create default unauthenticated role
    let unauthenticatedRole = await Role.findOne({where: {name: getEnv('UNAUTHENTICATED_ROLE')}});
    if (!unauthenticatedRole) {
      unauthenticatedRole = await Role.create({name: getEnv('UNAUTHENTICATED_ROLE'), editable: false});
      //-- grant basic permission
      let unauthenticatedPermissions = ['create_oauth_request', 'get_oauth_token', 'grant_owned_oauth_request'].map(permissionName => {
        let permission = permissions[permissionName];
        let permissionSettings = !!permission.permissionConditions && Object.keys(permission.permissionConditions).map(conditionName => {
          //-- permit without any condition
          return permission.permissionConditions[conditionName].permit();
        });
        return {
          roleId: unauthenticatedRole.id,
          permissionId: permissionName,
          permissionSettings: JSON.stringify(permissionSettings || []),
        }
      });
      await RolePermission.bulkCreate(unauthenticatedPermissions);
    }

    //-- create default super admin user if not existed
    let superAdminUser = await User.findOne({where: {username: getEnv('SUPER_ADMIN_USERNAME')}});
    if (!superAdminUser) {
      superAdminUser = await UserService.addUser({
        username: getEnv('SUPER_ADMIN_USERNAME'),
        authMethod: AUTH_METHODS.AUTH_PASSWORD,
        authData: [getEnv('DEFAULT_SUPER_ADMIN_PASSWORD')]
      });
      if (!superAdminUser.success)
        return superAdminUser;
      superAdminUser = superAdminUser.result;
    }
    let superAdminUserRole = await UserRole.findOne({
      where: {
        userId: superAdminUser.id,
        roleId: superAdminRole.id
      }
    });
    if (!superAdminUserRole) {
      await UserRole.create({userId: superAdminUser.id, roleId: superAdminRole.id});
    }

    let unauthenticatedPermissions = await UserService.getPermissionsFromRoles({roles: [unauthenticatedRole.id]});
    if (!unauthenticatedPermissions.success)
      return unauthenticatedPermissions;
    UserService.unauthenticatedPermissions = unauthenticatedPermissions.result;
  }

  static async signUp({username, authMethod, authInfo}) {

    let user = await User.find({where: {username: username}});
    if (user)
      return jsonError(errors.DUPLICATED_ERROR);

    let userData = {
      user: {username: username},
      auth: {}
    };

    switch (authMethod) {
      case AUTH_METHODS.AUTH_PASSWORD: {
        userData.auth.authMethod = AUTH_METHODS.AUTH_PASSWORD;
        let passwordResult = await hashPassword(authInfo[0]);
        if (!passwordResult.success)
          return passwordResult;
        userData.auth.authInfo1 = passwordResult.result;
        break;
      }

      case AUTH_METHODS.AUTH_OAUTH: {
        userData.auth.authMethod = AUTH_METHODS.AUTH_OAUTH;

        switch (authInfo[1]) {
          case SUPPORTED_OAUTH_PROVIDERS.GOOGLE: {

            let result = await new Promise((resolve) => {
              this.ggClient.verifyIdToken({
                idToken: authInfo[0],
                audience: getEnv('GG_CLIENT_ID')
              }).then((err, login) => {
                if (err) {
                  logger.error('signUp', err);
                  return resolve(jsonError(errors.INVALID_OAUTH_TOKEN));
                }
                let payload = login.getPayload();
                //-- if the email has not been verified then reject
                if (!payload.email_verified)
                  return resolve(jsonError(errors.INVALID_OAUTH_TOKEN));
                return resolve(jsonSuccess(payload));
              });
            });

            if (!result.success)
              return result;

            userData.auth.authInfo1 = authInfo[1];
            userData.auth.authInfo2 = result.result.email;
            break;
          }

          default: {
            return jsonError(errors.OAUTH_PROVIDER_NOT_SUPPORTED);
          }
        }
        break;
      }

      default: {
        return jsonError(errors.INVALID_AUTH_METHOD);
      }
    }

    //-- create new user with the userData
    user = await User.create(userData.user);
    userData.auth.userId = user.id;
    await Auth.create(userData.auth);
    return jsonSuccess();
  }

  static async getRoles() {
    let roles = await Role.findAll();
    return jsonSuccess(roles);
  }

  static async getRole({roleId}) {
    let role = await Role.findById(roleId);
    if (!role) {
      return jsonError(errors.NOT_FOUND_ERROR);
    }
    return jsonSuccess(role);
  }

  static async updateProfilePhoto({userId, photo}) {
    let uploadParams = {
      bucket: getEnv('S3_BUCKET'),
      fileName: UserService.S3_PROFILE_PHOTO_FOLDER + '/' + userId,
      fileContent: photo
    };
    let result = await S3Service.uploadFileToS3(uploadParams);

    if (!result.success)
      return result;

    let filePath = uploadParams.bucket + '/' + uploadParams.fileName;
    result.result = {
      profilePhoto: filePath,
    };

    let user = await User.findOne({where: {id: userId}});
    await user.updateAttributes({profilePhoto: filePath});

    return result;
  }

  static async addRole({name, super_user, editable}) {
    let role = await Role.findOne({where: {name: name}});
    if (role)
      return jsonError(errors.DUPLICATED_ERROR);
    role = await Role.create({name: name, super_user: super_user, editable: editable});
    return jsonSuccess(role);
  }

  static async updateRole({roleId, name, super_user, editable}) {
    let role = await Role.findOne({where: {id: roleId}});
    if (!role)
      return jsonError(errors.NOT_FOUND_ERROR);
    let updateRole = await Role.findOne({where: {name: name, id: {[Op.ne]: roleId}}});
    if (updateRole)
      return jsonError(errors.DUPLICATED_ERROR);

    await role.updateAttributes({name: name, super_user: super_user, editable: editable});
    return jsonSuccess();
  }

  static async deleteRole({roleId}) {
    let role = await Role.findOne({where: {id: roleId}});
    if (!role)
      return jsonError(errors.NOT_FOUND_ERROR);
    await RolePermission.destroy({where: {roleId: roleId}});
    await UserRole.destroy({where: {roleId: roleId}});
    await role.destroy();
    return jsonSuccess();
  }

  static async listRolePermissions({roleId}) {
    let role = await Role.findById(roleId);
    if (!role)
      return jsonError(errors.NOT_FOUND_ERROR);
    let rolePermission = await RolePermission.findAll({where: {roleId: roleId}});
    return jsonSuccess(rolePermission);
  }

  static async listAllRolePermission() {
    let rolePermissions = Object.keys(permissions).map((name, index) => {
      let listRolePermission = {};
      listRolePermission.id = index;
      listRolePermission.name = name;
      return listRolePermission;
    });
    return jsonSuccess(rolePermissions);
  }

  static async addRolePermission({roleId, permissionId, permissionSettings}) {
    let role = await Role.findOne({where: {id: roleId}});
    if (!role)
      return jsonError(errors.NOT_FOUND_ERROR);

    let rolePermission = await RolePermission.create({
      roleId: roleId,
      permissionId: permissionId,
      permissionSettings: permissionSettings
    });
    return jsonSuccess(rolePermission);
  }

  static async deleteRolePermission({rolePermissionId}) {
    let rolePermission = await RolePermission.findOne({where: {id: rolePermissionId}});
    if (!rolePermission)
      return jsonError(errors.NOT_FOUND_ERROR);
    await rolePermission.destroy();
    return jsonSuccess();
  }

  static async getRolesFromUser({userId, info}) {
    let roles = await UserRole.findAll({
      where: {userId: userId},
      attributes: info && info.length ? info : null,
      include: [{model: Role, as: 'role'}]
    });
    if (info)
      return jsonSuccess(roles.map(r => {
        let result = {};
        info.forEach(field => {
          result[field] = r[field];
        });
        return result;
      }));
    return jsonSuccess(roles);
  }

  static async getPermissionsFromRoles({roles}) {
    let permissions = await RolePermission.findAll({where: {roleId: {[Op.in]: roles}}});
    return jsonSuccess(permissions.map(p => {
      try {
        return {
          permissionId: p.permissionId,
          permissionSettings: p.permissionSettings ? JSON.parse(p.permissionSettings) : []
        }
      }
      catch (err) {
        console.log(p.permissionSettings);
        return null;
      }
    }));
  }

  static async getUserInfo({userId, info}) {
    let user = await User.findOne({where: {id: userId}, attributes: info && info.length ? info : null});
    if (!user)
      return jsonError(errors.USER_NOT_FOUND);
    user.dataValues.roles = await UserRole.findAll({
      where: {userId: userId},
      include: [{model: Role, as: 'role'}]
    }).map(userRole => {
      return userRole['role']
    });
    return jsonSuccess(user);
  }

  static async updateUserInfo({userId, firstName, lastName, birthday, gender, phoneNumber, email, profilePhoto}) {
    let user = await User.findById(userId);
    if (!user)
      return jsonError(errors.USER_NOT_FOUND);

    await user.updateAttributes({firstName, lastName, birthday, gender, phoneNumber, email, profilePhoto});
    return jsonSuccess();
  }

  static async updateUserPassword({userId, oldPassword, newPassword}) {
    // Check user and auth existence
    let user = await User.findById(userId);
    if (!user)
      return jsonError(errors.USER_NOT_FOUND);
    let auth = await Auth.findOne({
      where: {
        userId: userId,
        authMethod: AUTH_METHODS.AUTH_PASSWORD
      }
    });
    if (!auth)
      return jsonError(errors.NOT_FOUND_ERROR);

    let compareResult = await comparePassword(oldPassword, auth.authInfo1);
    if (!compareResult.success)
      return compareResult;
    if (!compareResult.result)
      return jsonError(errors.WRONG_PASSWORD_ERROR);

    let passwordResult = await hashPassword(newPassword);
    if (!passwordResult.success)
      return passwordResult;
    await auth.updateAttributes({authInfo1: passwordResult.result});
    return jsonSuccess();
  }

  static async promoteUser({userId, roles}) {
    //check user and role existed
    let user = await User.findById(userId);
    if (!user)
      return jsonError(errors.USER_NOT_FOUND);
    for (let i = 0; i < roles.length; i++) {
      let role = await Role.findById(roles[i]);
      if (!role) {
        return jsonError(errors.NOT_FOUND_ERROR);
      }
    }

    // Handle update roles for user
    let userRoles = await UserRole.findAll({where: {userId: userId}});
    let rolesNeedDelete = userRoles.filter(userRole => roles.indexOf(userRole.roleId) === -1);
    rolesNeedDelete.forEach(async role => {
      await role.destroy();
    });
    let rolesNeedAdd = roles
      .filter(role => userRoles
        .map(userRole => userRole.roleId)
        .indexOf(role) === -1)
      .map(x => {
        let role = {};
        role.userId = userId;
        role.roleId = x;
        return role;
      });
    userRoles = await UserRole.bulkCreate(rolesNeedAdd);
    return jsonSuccess();
  }

  static async getUsers({username, isActive, roleName}) {
    if (!username) username = "";
    let whereCondition = {username: {like: '%' + username + '%'}};
    if (isActive !== undefined) {
      whereCondition = {
        [Op.and]: [
          {username: {like: '%' + username + '%'}},
          {isActive: isActive},
        ]
      };
    }
    let users = await User.findAll({where: whereCondition});
    for (let i = 0; i < users.length; i++) {
      users[i].dataValues.roles = await UserRole.findAll({
        where: {userId: users[i].id},
        attributes: ['id', 'roleId', 'userId'],
        include: [{model: Role, as: 'role'}]
      }).map(x => {
        return x['role']['name'];
      });
    }
    if (roleName && roleName.length > 0) {
      users = users.filter(user => {
        let roles = user.dataValues.roles;
        roles = roles.filter(role => role.indexOf(roleName) > -1);
        return roles.length > 0;
      });
    }
    return jsonSuccess(users);
  }

  static async addUser({username, authMethod, authData}) {

    let user = await User.findOne({where: {username: username}});
    if (user)
      return jsonError(errors.DUPLICATED_ERROR);

    let userData = {
      user: {username: username},
      auth: {}
    };

    switch (authMethod) {
      case AUTH_METHODS.AUTH_PASSWORD: {
        userData.auth.authMethod = AUTH_METHODS.AUTH_PASSWORD;
        let passwordResult = await hashPassword(authData[0]);
        if (!passwordResult.success)
          return passwordResult;
        userData.auth.authInfo1 = passwordResult.result;
        break;
      }

      case AUTH_METHODS.AUTH_OAUTH: {
        userData.auth.authMethod = AUTH_METHODS.AUTH_OAUTH;

        switch (authData[1]) {
          case SUPPORTED_OAUTH_PROVIDERS.GOOGLE: {
            let result = await new Promise((resolve) => {
              UserService.ggClient.verifyIdToken({
                idToken: authData[0],
                audience: getEnv('GG_CLIENT_ID')
              }).then((err, login) => {
                if (err) {
                  logger.error('signUp');
                  return resolve(jsonError(errors.INVALID_OAUTH_TOKEN));
                }
                let payload = login.getPayload();
                //-- if the email has not been verified then reject
                if (!payload.email_verified)
                  return resolve(jsonError(errors.INVALID_OAUTH_TOKEN));
                return resolve(jsonSuccess(payload));
              });
            });

            if (!result.success)
              return result;

            userData.auth.authInfo1 = authData[1];
            userData.auth.authInfo2 = result.result.email;
            break;
          }

          default: {
            return jsonError(errors.OAUTH_PROVIDER_NOT_SUPPORTED);
          }
        }
        break;
      }

      default: {
        return jsonError(errors.INVALID_AUTH_METHOD);
      }
    }

    //-- create new user with the userData
    user = await User.create(userData.user);
    userData.auth.userId = user.id;
    await Auth.create(userData.auth);
    return jsonSuccess(user);
  }

  static async updateUser({userId, username, password, isActive}) {
    // Find, check username
    let user = await User.findOne({where: {id: userId}});
    if (!user)
      return jsonError(errors.USER_NOT_FOUND);
    let userTemp = await User.findOne({
      where: {username: username, id: {[Op.ne]: userId}}
    });
    if (userTemp) {
      return jsonError(errors.DUPLICATED_ERROR);
    }
    let auth = await Auth.findOne({where: {id: userId}});
    if (!auth)
      return jsonError(errors.AUTH_NOT_FOUND);

    // Update info
    await user.updateAttributes({username: username, isActive: isActive});
    if (password.length > 0) {
      let passwordResult = await hashPassword(password);
      if (!passwordResult.success)
        return passwordResult;
      await auth.updateAttributes({authInfo1: passwordResult.result});
    }
    return jsonSuccess();
  };

  static async deleteUser({userId}) {
    let user = await User.findOne({where: {id: userId}});
    if (!user)
      return jsonError(errors.USER_NOT_FOUND);
    await Auth.destroy({where: {userId: userId}});
    await UserRole.destroy({where: {userId: userId}});
    await User.destroy({where: {id: userId}});
    return jsonSuccess();
  }

  static async login({authData, authMethod}) {

    let user = await User.findOne({where: {username: authData[0]}});
    if (!user)
      return jsonError(errors.USER_NOT_FOUND);

    let auth = await Auth.findOne({
      where: {
        userId: user.id,
        authMethod: authMethod,
      }
    });

    if (!auth)
      return jsonError(errors.AUTH_NOT_FOUND);

    switch (authMethod) {
      case AUTH_METHODS.AUTH_OAUTH: {
        if (authMethod !== auth.authInfo1)
          return jsonError(errors.AUTH_NOT_FOUND);

        switch (authData[2]) {
          case 'google':
            let verifyTokenResult = await new Promise((resolve) => {
              UserService.ggClient.verifyIdToken({
                idToken: authData[1],
                audience: getEnv('GG_CLIENT_ID')
              }).then((err, login) => {
                if (err)
                  return resolve(jsonError(errors.INVALID_OAUTH_TOKEN));
                if (login.getPayload().email !== auth.authInfo2)
                  return resolve(jsonError(errors.INVALID_OAUTH_TOKEN));
                return resolve(jsonSuccess());
              });
            });
            if (!verifyTokenResult.success)
              return verifyTokenResult;

            return jsonSuccess(user);
        }
        break;
      }

      case AUTH_METHODS.AUTH_PASSWORD: {

        let compareResult = await comparePassword(authData[1], auth.authInfo1);
        if (!compareResult.success)
          return compareResult;

        if (!compareResult.result)
          return jsonError(errors.WRONG_PASSWORD_ERROR);

        return jsonSuccess(user);
      }
    }
  }
}

export {UserService, SUPPORTED_OAUTH_PROVIDERS};


