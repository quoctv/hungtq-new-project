import {errors, jsonError, logger} from '../utils/system';
import {valueRequired} from '../middleware/validators';
import {UserService} from '../services/UserService';
import {editableFields, regexStringValue, subset} from '../controllers/RPCControler';
import {OAuthService} from '../services/OAuthService';

const RPCController = require('express').Router();

const middlewareRun = (middleware, req, res, cb) => {
  if (!middleware || !middleware.length)
    return cb();
  
  let i = 0;
  let process = (layer) => {
    return layer(req, res, () => {
      setImmediate(() => {
        i++;
        if (res.headersSent)
          return;
        if (i === middleware.length)
          return cb();
        return process(middleware[i]);
      });
    });
  };
  process(middleware[i]);
};

const rpcConditions = {
  regexStringValue: (fields) => {
    return {
      fields,
      valueType: 'string',
      permit: (conditions) => {
        return {
          name: 'regexStringValue', value: !!conditions && conditions.map(c => {
            return {name: c.name, value: c.value}
          })
        }
      },
      evaluate: (params, conditions) => {
        // console.log(fields, params, conditions);
        return !fields.some(f => {
          return !(!conditions || (!!params[f] && params[f].length && params[f].every(field => true)));
        });
      },
    };
  },
  editableFields: (fields) => {
    return {
      fields,
      valueType: 'boolean',
      permit: (conditions) => {
        return {name: 'editableFields', value: conditions || null}
      },
      evaluate: (params, conditions) => {
        return true;
      },
    };
  },
  subset: (fields) => {
    return {
      fields,
      valueType: 'string',
      permit: (conditions) => {
        return {name: 'subset', value: conditions || null}
      },
      evaluate: (params, conditions) => {
        return !fields.some(f => {
          return !(!conditions || (!!params[f] && params[f].length && params[f].every(field => conditions.indexOf(field) >= 0)));
        });
      },
    }
  },
};

const rpcActions = {
  
  'get_role': {
    params: [
      {name: 'roleId', type: 'number'}
    ],
    paramsValidations: [
      valueRequired({attributes: ['roleId']})
    ],
    action: async (principal, params) => {
      return UserService.getRole({roleId: params.roleId});
    }
  },
  
  'create_role': {
    params: [
      {name: 'name', type: 'string'},
      {name: 'super_user', type: 'boolean'},
      {name: 'editable', type: 'boolean'}
    ],
    paramsValidations: [
      valueRequired({attributes: ['name']})
    ],
    action: async (principal, params) => {
      return UserService.addRole({
        name: params.name,
        super_user: params.super_user,
        editable: params.editable
      });
    }
  },
  
  'update_role': {
    params: [
      {name: 'roleId', type: 'number'},
      {name: 'name', type: 'string'},
      {name: 'super_user', type: 'boolean'},
      {name: 'editable', type: 'boolean'}
    ],
    paramsValidations: [
      valueRequired({attributes: ['roleId']})
    ],
    action: async (principal, params) => {
      return UserService.updateRole({
        roleId: params.roleId,
        name: params.name,
        super_user: params.super_user,
        editable: params.editable
      });
    }
  },
  
  'remove_role': {
    params: [
      {name: 'roleId', type: 'number'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['roleId']})
    ],
    action: async (principal, params) => {
      return UserService.deleteRole({
        roleId: params.roleId
      });
    },
    log: true
  },
  
  'list_roles': {
    action: async (principal) => {
      return UserService.getRoles();
    }
  },
  
  'add_role_permission': {
    params: [
      {name: 'roleId', type: 'number'},
      {name: 'permissionId', type: 'string'},
      {name: 'permissionConditions', type: 'string'}
    ],
    paramsValidations: [
      valueRequired({attributes: ['roleId', 'permissionId']})
    ],
    action: async (principal, params) => {
      return UserService.addRolePermission({
        roleId: params.roleId,
        permissionId: params.permissionId,
        permissionSettings: params.permissionConditions
      });
    }
  },
  
  'remove_role_permission': {
    params: [
      {name: 'rolePermissionId', type: 'number'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['permission_id']})
    ],
    action: async (principal, params) => {
      return UserService.deleteRolePermission({
        rolePermissionId: params.rolePermissionId
      });
    },
    log: true,
  },
  
  'list_role_permissions': {
    params: [
      {name: 'roleId', type: 'number'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['roleId']})
    ],
    action: async (principal, params) => {
      return UserService.listRolePermissions({
        roleId: params.roleId
      });
    }
  },
  
  'list_all_role_permission': {
    action: async () => {
      return UserService.listAllRolePermission();
    }
  },
  
  'create_user': {
    params: [
      {name: 'username', type: 'string'},
      {name: 'password', type: 'string'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['username', 'password']})
    ],
    permissionConditions: {
      regexStringValue: rpcConditions.regexStringValue(['username'])
    },
    action: async (principal, params) => {
      return UserService.addUser({
        username: params.username,
        authMethod: Auth.constants.AUTH_METHODS.AUTH_PASSWORD,
        authData: [params.password]
      });
    },
    log: true,
  },
  
  'update_user': {
    params: [
      {name: 'userId', type: 'number'},
      {name: 'username', type: 'string'},
      {name: 'password', type: 'string'},
      {name: 'isActive', type: 'boolean'},
      {name: 'password_change_required', type: 'boolean'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['userId']}),
    ],
    permissionConditions: {
      editableFields: rpcConditions.editableFields(['username', 'password', 'isActive'])
    },
    log: true,
    action: async (principal, params) => {
      return UserService.updateUser({
        userId: params.userId,
        username: params.username,
        password: params.password,
        isActive: params.isActive,
        password_change_required: params.password_change_required
      });
    }
  },
  
  'remove_user': {
    params: [
      {name: 'userId', type: 'number'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['userId']}),
    ],
    log: true,
    action: async (principal, params) => {
      return UserService.deleteUser({userId: params.userId});
    }
  },
  
  'list_users': {
    params: [
      {name: 'username', type: 'string'},
      {name: 'isActive', type: 'boolean'},
      {name: 'role', type: 'array'},
    ],
    action: async (principal, params) => {
      if (!params)
        params = {};
      return UserService.getUsers({username: params.username, isActive: params.isActive, roleName: params.role})
    }
  },
  
  'update_user_info': {
    params: [
      {name: 'userId', type: 'number'},
      {name: 'firstName', type: 'string'},
      {name: 'lastName', type: 'string'},
      {name: 'birthday', type: 'number'},
      {name: 'gender', type: 'number'},
      {name: 'email', type: 'string'},
      {name: 'phoneNumber', type: 'string'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['userId']}),
    ],
    permissionConditions: {
      editableFields: rpcConditions.editableFields(['firstName', 'lastName', 'birthday', 'gender', 'phoneNumber', 'email', 'profilePhoto'])
    },
    action: async (principal, params) => {
      return UserService.updateUserInfo({
        userId: params.userId,
        firstName: params.firstName,
        lastName: params.lastName,
        birthday: params.birthday,
        gender: params.gender,
        phoneNumber: params.phoneNumber,
        email: params.email,
        profilePhoto: params.profilePhoto,
      })
    },
    log: true,
  },
  
  'read_user_info': {
    params: [
      {name: 'userId', type: 'number'},
      {name: 'info', type: 'array'}
    ],
    paramsValidations: [
      valueRequired({attributes: ['userId']}),
    ],
    permissionConditions: {
      subset: rpcConditions.subset(['info'])
    },
    action: async (principal, params) => {
      return UserService.getUserInfo({userId: params.userId, info: params.info});
    }
  },
  
  'promote_user': {
    params: [
      {name: 'userId', type: 'number'},
      {name: 'roles', type: 'array'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['userId']}),
    ],
    permissionConditions: {
      subset: rpcConditions.subset(['roles'])
    },
    log: true,
    action: async (principal, params) => {
      return UserService.promoteUser({
        userId: params.userId,
        roles: params.roles
      });
    }
  },
  
  'get_owned_roles': {
    action: async (principal) => {
      return UserService.getRolesFromUser({userId: principal.userInfo.owner});
    }
  },
  
  'update_owned_password': {
    params: [
      {name: 'oldPassword', type: 'string'},
      {name: 'newPassword', type: 'string'},
    ],
    paramsValidations: [
      valueRequired(({attributes: ['oldPassword', 'newPassword']}))
    ],
    action: async (principal, params) => {
      return UserService.updateUserPassword({
        userId: principal.userInfo.owner,
        oldPassword: params.oldPassword,
        newPassword: params.newPassword
      });
    }
  },
  
  'update_owned_profile_photo': {
    params: [
      {name: 'photo', type: 'file'},
    ],
    action: async (principal, params) => {
      return UserService.updateProfilePhoto({
        userId: principal.userInfo.owner,
        photo: params.photo.data
      });
    }
  },
  
  'update_owned_info': {
    params: [
      {name: 'firstName', type: 'string'},
      {name: 'lastName', type: 'string'},
      {name: 'birthday', type: 'number'},
      {name: 'gender', type: 'number'},
      {name: 'email', type: 'string'},
    ],
    permissionConditions: {
      editableFields: rpcConditions.editableFields(['firstName', 'lastName', 'birthday', 'gender', 'email', 'profilePhoto'])
    },
    action: async (principal, params) => {
      return UserService.updateUserInfo({
        userId: principal.userInfo.owner,
        firstName: params.firstName,
        lastName: params.lastName,
        birthday: params.birthday,
        gender: params.gender,
        email: params.email,
        profilePhoto: params.profilePhoto,
      })
    }
  },
  
  'read_owned_info': {
    params: [
      {name: 'userId', type: 'number'},
      {name: 'info', type: 'array'}
    ],
    permissionConditions: {
      subset: rpcConditions.subset(['info'])
    },
    action: async (principal, params) => {
      return UserService.getUserInfo({userId: principal.userInfo.owner, info: params.info});
    }
  },
  
  'list_operation_logs': {
    params: [
      {name: 'from', type: 'number'},
      {name: 'to', type: 'number'}
    ],
    action: () => {
    
    }
  },
  
  'create_oauth_app': {
    params: [
      {name: 'name', type: 'string'},
      {name: 'callbackUrls', type: 'array'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['name', 'callbackUrls']})
    ],
    action: async (principal, params) => {
      return OAuthService.createOAuthApp({
        userId: principal.userInfo.owner,
        name: params.name,
        callbackUrls: params.callbackUrls
      });
    }
  },
  
  'list_owned_oauth_apps': {
    params: [
      {name: 'info', type: 'array'},
    ],
    paramsValidations: [],
    permissionConditions: {
      subset: rpcConditions.subset(['info'])
    },
    action: async (principal, params) => {
      return await OAuthService.getAllOAuthApps({
        userId: principal.userInfo.owner,
        info: params.info
      });
    }
  },
  
  'update_owned_oauth_app': {
    params: [
      {name: 'appId', type: 'string'},
      {name: 'name', type: 'string'},
      {name: 'callbackUrls', type: 'array'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['appId']})
    ],
    action: async (principal, params) => {
      let oauthApp = await OAuthService.getOAuthApp({appId: params.appId});
      if (!oauthApp.success)
        return oauthApp;
      oauthApp = oauthApp.result;
      
      if (oauthApp.userId !== principal.userInfo.owner)
        return jsonError(errors.RESOURCE_NOT_OWNED);
      
      return OAuthService.updateOAuthApp({
        appId: params.appId,
        name: params.name,
        callbackUrls: params.callbackUrls
      });
    }
  },
  
  'remove_owned_oauth_app': {
    params: [
      {name: 'appId', type: 'string'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['appId']})
    ],
    action: async (principal, params) => {
      let app = await OAuthService.getOAuthApp({appId: params.appId});
      if (!app.success)
        return app;
      if (app.result.userId !== principal.userInfo.owner)
        return jsonError(errors.RESOURCE_NOT_OWNED);
      return OAuthService.removeOAuthApp({appId: params.appId});
    }
  },
  
  'get_owned_oauth_app': {
    params: [
      {name: 'appId', type: 'string'},
      {name: 'info', type: 'string'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['appId']})
    ],
    permissionConditions: {
      subset: rpcConditions.subset(['info'])
    },
    action: async (principal, params) => {
      let oauthApp = await OAuthService.getOAuthApp({appId: params.appId});
      if (!oauthApp.success)
        return oauthApp;
      oauthApp = oauthApp.result;
      
      if (oauthApp.userId !== principal.userInfo.owner)
        return jsonError(errors.RESOURCE_NOT_OWNED);
      return jsonSuccess(oauthApp);
    }
  },
  
  'get_oauth_app': {
    params: [
      {name: 'appId', type: 'string'},
      {name: 'info', type: 'array'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['appId']})
    ],
    permissionConditions: {
      subset: rpcConditions.subset(['info'])
    },
    action: async (principal, params) => {
      return OAuthService.getOAuthApp({appId: params.appId, info: params.info});
    }
  },
  
  'get_oauth_token': {
    params: [
      {name: 'grantType', type: 'string'},
      {name: 'grantData', type: 'array'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['grantType', 'grantData']})
    ],
    action: async (principal, params) => {
      return OAuthService.getOAuthToken({grantType: params.grantType, grantData: params.grantData});
    }
  },
  
  'create_oauth_request': {
    params: [
      {name: 'appId', type: 'string'},
      {name: 'userId', type: 'string'},
      {name: 'state', type: 'string'},
      {name: 'scope', type: 'string'},
      {name: 'callbackUrl', type: 'string'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['appId', 'userId', 'scope', 'callbackUrl']})
    ],
    action: async (principal, params) => {
      return OAuthService.createOAuthRequest({
        appId: params.appId,
        userId: params.userId,
        state: params.state,
        scope: params.scope,
        callbackUrl: params.callbackUrl
      });
    }
  },
  
  'grant_owned_oauth_request': {
    params: [
      {name: 'requestId', type: 'string'},
      {name: 'isAccepted', type: 'string'},
    ],
    paramsValidations: [
      valueRequired({attributes: ['requestId', 'isAccepted']})
    ],
    action: async (principal, params) => {
      let request = await OAuthService.getOAuthRequest({requestId: params.requestId});
      if (!request.success)
        return request;
      request = request.result;
      
      if (request.userId !== principal.userInfo.owner)
        return jsonError(errors.RESOURCE_NOT_OWNED);
      
      return OAuthService.grantOAuthRequest({requestId: params.requestId, isAccepted: params.isAccepted});
    }
  },
};

RPCController.post('/rpc', (req, res) => {
  let principal = req.principal;
  
  let commandName = req.body.action;
  let params = req.body;
  
  if (!commandName || !rpcActions[commandName])
    return res.json(jsonError(errors.MALFORMED_REQUEST_ERROR));
  
  //-- param type check
  //-- TODO
  
  //-- param validation check
  middlewareRun(rpcActions[commandName].paramsValidations, req, res, () => {
    //-- permission check
    let permissions = principal['permissions'].filter((p) => p && (p.permissionId === commandName));
    if (!permissions.length)
      return res.json(jsonError(errors.ACCESS_DENIED));
    
    let conditionCheck = permissions.some((p) => {
      let conditionFailed = Array.isArray(p.permissionSettings) && p.permissionSettings.some(setting => {
        return !setting || !rpcActions[commandName].permissionConditions[setting.name].evaluate(params, setting.value);
      });
      return !conditionFailed;
    });
    
    if (!conditionCheck)
      return res.json(jsonError(errors.ACCESS_CONDITION_FAILED));
    
    if (!rpcActions[commandName].action)
      return res.json(jsonError(errors.NOT_IMPLEMENTED_ERROR));
    
    rpcActions[commandName].action(principal, params)
      .then((result) => {
        res.json(result);
      })
      .catch((err) => {
        logger.error(`/rpc ${commandName}`, err);
        res.json(jsonError(errors.SYSTEM_ERROR));
      });
  });
});

export {RPCController, rpcActions, editableFields, subset, regexStringValue};
