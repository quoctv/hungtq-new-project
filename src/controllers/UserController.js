import {errors, jsonError, logger} from '../utils/system';
import {UserService} from '../services/UserService';

const UserController = require('express').Router();

//-- controller's definition here
UserController.post('/register', (req, res) => {
  UserService.signUp(req.body)
    .then((result) => {
      res.json(result);
    })
    .catch((err) => {
      logger.error(err);
      res.json(jsonError(errors.SYSTEM_ERROR));
    });
});

//--- export the controller and mount at the mount point
export {UserController};
