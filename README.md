# Claire Framework v2.0
NodeJs framework trên nền ExpressJs.

### Claire là chi?
Claire dịch từ tiếng Pháp là 'rõ ràng'. Mình không đặt là Clear để tránh trùng tên với dầu gội đầu.
Framework vẫn sẽ giữ cái tên này cho đến khi có ai đó nghĩ ra một cái tên hay hơn.

### Framework này có gì hay ho?
- Framework nhẹ nhàng, dễ dàng tuỳ chỉnh. Mọi thứ đều được tận dụng từ ExpressJs.
- Hỗ trợ cú pháp import/export của ES6. Cú pháp này rất tiện sử dụng.
- Tổ chức cấu hình (configuration) và môi trường (environment) rõ ràng.
- Hỗ trợ cả NoSQL (MongoDb) và SQL (MySQL, Postgres...) với giao diện thống nhất.
- Hỗ trợ database migration trong trường hợp sử dụng SQL.

### Làm như nào để chạy
- `git clone`
- `npm install && npm audit fix`

Có 3 chế độ chạy:
- chạy dev: `npm start`
- chạy dev theo dõi thay đổi: `npm run watch`
- build distribution: `npm run build`
- chạy distribution bằng: `npm serve`

Xem file package.json để hiểu các chế độ tương tứng.
> Lưu ý: khi tạo database migration, không sử dụng chế độ watch, vì khi file migration trống được tạo ra sẽ chạy luôn, dẫn đến các thay đổi về sau không được áp dụng.

### Kiến trúc Claire Framework
Claire được viết bằng cú pháp ES6 nên không thể chạy trực tiếp trên NodeJs mà phải được biên dịch bằng babel.

Cấu trúc thư mục:
- src: thư mục chứa các code mà người lập trình sẽ thay đổi
- dist: thư mục này được tạo ra khi babel dịch code trong thư mục src
- env: thư mục chứa các biến môi trường. LCL là môi trường mặc định của Claire, tương ứng với môi trường làm việc local tại PC của developer.
- package.json, package-lock.json, node_modules: dùng chung cho src và dist
- file tiện ích sequelizer.sh
- doc: chứa các tài liệu hướng dẫn, demo code
- README.md và .gitignore 

### Trình tự framework khởi động:
- load tất cả các biến môi trường
- pre-boot: kết nối CSDL và khởi tạo các model
- boot các services được liệt kê trong `src/core/framework.js` theo thứ tự
- tạo Express app và đưa vào các middleware theo thứ tự: các middleware config, regulators
- mount các controller được liệt kê trong `src/core/framework.js` theo thứ tự 
- lắng nghe kết nối

> Trong Claire các service được khai báo là các module độc lập xử lí logic. 
Tất cả các hàm trong service đều được khai báo tĩnh.
Các controller sẽ gọi đến các hàm của các service khác nhau để xử lí công việc.
Service KHÔNG NÊN gọi lẫn nhau để tránh ràng buộc logic.

![Claire architecture](doc/claire.png?raw=true "Claire")

### Database ORM
Trong source code mẫu, cả MongoDB (m) và MySQLongoose (sequelize) đều được sử dụng. Trong thực tế chỉ dùng 1 trong 2 DBMS này thôi.
Tuy nhiên để tạo giao diện thống nhất cho framework, các hàm định nghĩa model dùng chung một signature.
Phần này có thể tuỳ chỉnh lại trong preBoot (thật ra thì phần nào cũng có thể tuỳ chỉnh được).

### Mongoose
MongoDB không có khái niệm migration. Xem document của [Mongoose](https://mongoosejs.com/docs/).

### Sequelize
Sequelize là ORM dùng trong trường hợp sử dụng SQL. Xem document của [Sequelize](http://docs.sequelizejs.com/).
Trong Claire, Sequelize được cấu hình sẵn để đọc các biến môi trường tương ứng.
Để chạy các lệnh của Sequelize, chạy file `sequelizer.sh` và cấp các thông số tương tự như sequelize-cli
Ví dụ:
- `./sequelizer db:migrate`
- `./sequelizer migration:generate --env=DEV`
Nếu --env không được cung cấp thì mặc định là `--env=LCL`
